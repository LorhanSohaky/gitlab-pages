#!/bin/sh

mkdir -p .public
rm .public/* -f

current_date=$(TZ=America/Sao_Paulo date +"%Y-%m-%d")
string="## $current_date\n### Changed \n"

for x in `git diff --staged --name-only | grep -v "CHANGELOG.md" | egrep ".md$"`
do
  file_name=$( basename $x | sed 's/\.[^.]*$//')
  showdown makehtml -i $file_name.md -o .public/$file_name.html
  SHA_FILE=$(sha256sum .public/$file_name.html | awk '{print $1}')
  string="$string- $file_name.md ($SHA_FILE)\n"
done

echo $string | cat - CHANGELOG.md > tmp && mv tmp CHANGELOG.md