#!/bin/sh

status=1
has_md=0

for file in $(git diff --staged --name-only)
do
  if [ "$file" = "CHANGELOG.md" ]; then
    status=0
  elif [ $(mimetype -b $file) = "text/markdown" ]; then
    has_md=1
  fi
done

if [ $status = 1 ] && [ $has_md = 1 ]; then
  ./shell.sh
  exit 1
fi

exit 0